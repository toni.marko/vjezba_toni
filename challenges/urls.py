# kreirali file naknadno na dodavanje prvog viewa (chapter 3. korak 22.)

from django.urls import path

from . import views

# sada kreiramo novu listu urlpatterns dodajemo path
# path prima dva argumenta, prvi je string koji opisuje url koji zelimo supportat
# drugi argument je pokazivac na view funkciju kloja bi se trebala executati kada request dode na taj url


urlpatterns = [
    

    path("", views.index, name="index"),  #ch3. k30, dodajemo path na kojem ce nam izlistati svaki mjesec za koji imamo poruku i gdje klikom biramo koji mjesec da otvorimo
                     # ! u views.py u ovom koraku kreiramo i view za ovaj path i nakon toga ga tu navedem (views.index)
               # path je prazan ("") zato sto ova lista urlpatterns se dohvaca za requestove poslane na challenges/ 
               #(tako je navedeno u pathu u urls.py fileu u monthly_challenges direktoriju),
               # za te requestove ucitavamo url-ove iz challenges.urls foldera,
               #zato kad je path prazan("") znaci da ce se otvoriti samo /challenges/ sto i trebamo dobiti

               #ch4. k45, dodan name="index" kako bi mogli koristiti url tag za link i path koji opet prikaze sve mjesece


    path("<int:month>", views.monthly_challenge_by_number), #ch 3, korak 25 nakon views.py, dodajemo novi path, 
                                                            #koji preusmjerava u novi view samo ako je naveden broj umjesto stringa

    #redoslijed je bitan, prvo zelimo provjeriti je li broj, a zatim jel string

    path("<str:month>", views.monthly_challenge, name="month-challenge")  # za chapter3. korak 24., dinamicko, koristimo placeholder <> umjesto hardcoded teksta
                                                # str: ispred dinamickog identifikatora segmenta (month) govori djangu da vrijednost unesena tamo
                                                # treba biti tretirana kao string i konvertirana u string  -- dodano u ch3. korak 25, sada ici u views.py
                                              # sada cu zakomentirat ostale pathove, jer je dovoljan ovaj dinamicki sa dinamickim path segmentom
                                              # i uvijek ce se triggerati samo monthly_challenges funkcija u views.py

                                              # ch3. k 28, dodajemo imena u path kako bi ih dinamicki mogli koristiti i mijenjati
                                              # ide keyword name i onda ="ime", 
                                              #sada kad url ima ime, to ime koristimo da kreiramo pathove (url-ove)
                                              # koji pokazuju na to registrirano url ime - idemo na views.py
]
# uz to, moramo jos povezati challenges app sa cijelim projektom
# moramo napraviti cijeli projektni url config svjestan da postoji url config za ovaj specificni app
# zato idemo na urls.py file u monthly challenges folderu jer je to main folder