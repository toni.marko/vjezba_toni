from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect, Http404
from django.template.loader import render_to_string

from django.urls import reverse

#ch3. korak 26
monthly_challenges = {    # rijecnik koji cemo koristiti dolje u funkciji, zamjenit cemo kod u funkciji i if else statemente
    "january" : "Eat no meat for the entire month!",
    "february": "Walk for atleast 20 minutes every day",
    "march" :  "Learn Django for at least 20 minutes every day!",
    "april" : "Eat no meat for the entire month!",
    "may": "Walk for atleast 20 minutes every day",
    "june" :  "Learn Django for at least 20 minutes every day!",
    "july" : "Eat no meat for the entire month!",
    "august": "Walk for atleast 20 minutes every day",
    "september" :  "Learn Django for at least 20 minutes every day!",
    "october" : "Eat no meat for the entire month!",
    "november" : "Walk for atleast 20 minutes every day",
    "december" :  None
}

# Create your views here.


 #ch3. korak 30, kreiram view za prazan path u urls.py koji sam sada napravio
def index(request):
   # list_items = ""  #nova varijabla, u pocetku prazan string
    months = list(monthly_challenges.keys())  #iskopirano iz def monthly_challenge, daje listu keyeva za mjesece

    #for month in months:
       # capitalized_month = month.capitalize()
       # month_path = reverse("month-challenge", args=[month])
       # list_items += f"<li><a href=\"{month_path}\">{capitalized_month}</a></li>" #ova linija koda prode kroz sve mjesece, tj keyeve u listi
                                                                                    # i generira novi string za svaki mjesec koji ima
                                                                                    #string koji sadrzi hardcoded list tag, i anchor tag(<a>)
                                                                                    #ali i dinamicki path na koji vodi navedeni link(href) i koji
                                                                                    #ima dinamicki tekst unutar linka.
                                                                                    #zatim taj link string dodamo u list_items string koji je inicijalno prazan string
        #uglavnom nakon zavrsetka for petlje ce to biti nit <li> koji ce sadrzavat linkove na svaki mjesec
   # response_data = f"<ul>{list_items}</ul>"
    #return HttpResponse(response_data)

    #ch4 k 40, zakomentirat cu vecinu funkcionalnosti iznad (for pretlja i response i return), jer kreiranje mjeseci prebacujem u index.html template

    #k40, umjesto zakomentiranog koristit cu render naredbu, koja prima request, path do template koji koristimo i kontekst
    return render(request, "challenges/index.html", {  #kontekstu moram proslijediti listu mjeseci, tako da ju tamo moze outputat
        "months" :months  #napravimo key "months" kojemu pridodamo vrijednost liste months koju generiramo nekoliko linija iznad (months = list(monthly_challenges.keys()))
    })                      #zatim idemo nazad u index template koristiti tu listu u <ul>


#ch3. korak 25, osiguravamo da moze biti broj umjesto imena mjeseca, u ovom koraku je samo kreirana funkcija, koja samo vraca broj mjeseca

#ch3. korak 27. nadogradujemo funkciju kako bi za brojeve vracala konkretan tekst
def monthly_challenge_by_number(request, month):    #prima request i broj mjeseca ako se unese - vezano uz path i int type podatka
    months = list(monthly_challenges.keys())   #k 27, govori na koji url ce forwardat ako je broj
                                               # ubiti ucitat ce iz rijecnika monthly_dictionary odgovor
                                             #.keys() nam vraca listu keyeva iz rijecnika, ide redom zbog novog pythona, ne treba sortirati rijecnik
    
    if month > len(months):                             #vraca eror ako je broj mjesec ubiti > 12
        return HttpResponseNotFound("Invalid month")
    
    redirect_month = months[month - 1]   #mjesec na koji forwarda je na onaj broj u listi koji se unese kao broj mjeseca u url path, npr za broj 5 forwarda na peto mjesto a to je may
                                        # -1 je zato sto je prvi element liste na poziciji 0  

    redirect_path = reverse("month-challenge", args=[redirect_month])  #ch 3, k28 - nastavak na urls.py gdje je dodano ime za path(month-challenge)
                                                # ova naredba sama otkriva kako ispravno kreirati kompletan path do url-a koji je naveden (month_challenge)
                                                # uz to sadrzi i args=[redirect_month] koji omogucuje da nakon /challenges/ navedemo i 
                                                # ime konkretnog mjeseca koji nas zanima
                                                #zato cu zakomentirati stari odgovor koji vraca red ispod
    
    

    return HttpResponseRedirect(redirect_path)

#chapter 3. korak 24          #month je dodano naknadno, vezano za <month> u urls.py

def monthly_challenge(request, month):   #ovo je ako zelimo dinamicku funkciju, znaci da ne moramo raditi za svaki mjesec
    try:
        challenge_text = monthly_challenges[month]               # bolji nacin je ovako, nego da radimo posebnu funkciju za svaki mjesec
        
        #response_data = render_to_string("challenges/challenge.html")   #ch3. k29 dodajemo html string da bude response
                                                       #u { } umecemo varijablu koju zelimo injektirati (challenge_text), to jos dodamo u novi odgovor, red ispod 
                                                       #u ch 4, k 34 nakon kreiranja challenges.html dosli smo tu i f"<h1>{challenge_text}</h1>" 
                                                       #smo zamjenili sa render_to_string("challenges/challenge.html")

        #return HttpResponse(response_data) # u k. 29 stavili (reponse_data) umjesto (challenge_text) kako je bilo do tada

        return render(request, "challenges/challenge.html", {  #ch4, k 35, zamjenili gornje dvije zakomentirane linije koda sa ovom jednom, #rade istu stvar samo je ovo bolje
                                                                #jedina razlika je sto ovdje treba navesti prvi argument (request) ispred imena templatea, znaci request ispred "challenges/challenge.html"
            
            "text": challenge_text,  #ch4, k36 dodali trecu varijablu i ovu liniju unutar {} kako bi poruke u html bile dinamicke a ne staticke, to je ubiti key-value par ("text" je key a value je stavljen challenge_text)
                                    #to je samo prvi korak, drugi korak je da koristimo ovaj key-value u template fileu (idemo u challenge.html)
            
            "month_name": month #ch4.,k 37, rjesenje zadatka, trebamo dodati naziv mjeseca, ali da ima veliko pocetno slovo,
                                            # mjesec svakako priamo kao varijablu u funkciji vec, sada idemo u template to dodati
                                            #ch4, k38,   .capitalize() je maknut nakon month i bit ce dodan u html templateu u obliku filtera
        })  
                                                   
    except:
        
        raise Http404() #ch4, k47, nastavno na 404.html template dodan je ovaj odgovor ukoliko ne postoji stranica da se raisa ovo
                        # zbog toga je zakomentiran red ispod.

        #return HttpResponseNotFound("<h1>This month is not supported!</h1>") #u k29. promjenjeno(dodano <h1></h1>) dotad je bilo ("This month is not supported")

             # posebnu funkciju. znaci odgovor ovisi o tome za koji mjesec je request
                                  # ali prvo idemo vidit kako mozemo povezat ovu funkciju -- idemo na urls.py  u challenges fileu

                                  #zbog ovog nam vise nisu potrebne pojedinacne funkcije gore, zakomentirat cu ih da i dalje vidim kod